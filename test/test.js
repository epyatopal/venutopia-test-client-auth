var assert = require("assert")
    , request = require("supertest")
    , config = require('../config/config.js').development
    , should = require('should')
    , http = require('http')
    , Browser = require("zombie")
    , app = require('../app.js');

process.env.NODE_ENV = 'mocha';


describe('#--Routing', function(){
    before(function() {
        this.server = http.createServer(app).listen(3000, function(){
            console.log('Express server listening on port 3000')
        });
        this.browser = new Browser({ site: 'http://localhost:3000' });
        request(this.server)
            .get('/admin/dropDatabase')
    });

    describe('#--contact page', function() {
        before(function(done) {
            this.browser.visit('/auth', done);
        });

        it('should show contact a form client', function(){
            assert.ok(this.browser.success);
            assert.equal(this.browser.text('a'), 'Login with Facebook Login with Google+c1oudbase.com');
            console.log( this.browser.html(".button"));
//            this.browser.clickLink(".button", function(e, browser, status) {
////                assert.equal(browser.location.pathname, '/auth/facebook');
//            });
        });


        it('should show contact a form', function(done){
            request(this.server)
            .get('/')
            .expect(302, done);
        });

    });


    var testUser = {}
        , testAdmin = {}
        , testCampaign = {}
        , testGame = {}
        , CookiesAdmin
        , CookiesUser


    describe('#--admin ', function(){

        describe('#--admin/login client', function(){
            before(function(done){
                this.browser.visit('/admin/login', done);
            })
            it('should show contact a form admin', function(){
                assert.ok(this.browser.success);
                assert.equal(this.browser.text('title'), 'Venutopia Admin Login');

            });
            it('should refuse empty name', function(done) {
                var browser = this.browser;
                browser.pressButton('#login').then(function() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('div.admin-login-error'), 'This is a required field.');
                }).then(done, done);
            });
            it('should refuse empty password', function(done) {
                var browser = this.browser;
                browser.fill('#inputName', "test")
//                console.log(browser.text('div.admin-login-error'));
                browser.pressButton('#login').then(function() {
                    assert.ok(browser.success);
                    assert.equal(browser.text('#adminLoginPassError'), 'This is a required field.');
                }).then(done, done);
            });
            it('should keep values on partial submissions', function(done) {
                var browser = this.browser;
                browser.fill('#inputName', "test")
                browser.fill('#inputPassword', "test");
                browser.pressButton('#login').then(function() {
                    assert.equal(browser.field('#inputName').value, 'test');
                    assert.equal(browser.field('#inputPassword').value, 'test');
                }).then(done, done);
            });

        })

        it(',redirect should show contact a form', function(done){
            request(this.server)
                .get('/admin')
                .expect(302, done);
        });

        it('create-user, should return error null ', function(done){
            request(this.server)
                .post('/admin/create-user')
                .send({name: 'mocha-test-admin', password: 'no password', role: 'Admin'})
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    result.text.should.not.be.equal('error');
                    testAdmin = {name: 'mocha-test-admin', password: 'no password', role: 'Admin'}
                    done()
                })
        })
        it('login, should return error null ', function(done){
            request(this.server)
                .post('/admin/login')
                .send({name: 'mocha-test-admin', password: 'no password'})
                .expect(200)
                .end(function(err, result){
//                    console.log('iiiiiiiiiiiii', result.headers['set-cookie'])
                    should.not.exist(err);
                    CookiesAdmin = result.headers['set-cookie'].pop().split(';')[0];
                    var _el = result.headers['set-cookie'].pop().split('%22')
                    testAdmin = {_id: _el[_el.length -2]}
                    done()
                })
        })

        it('campaign new, should return object', function(done){

            var req = request(this.server).post('/admin/campaigns/new')
            req.cookies = CookiesAdmin;
            req.send({
                title:"testTitle"
                , sponsor: 'testSponsor'
                , text: 'testText'
                , pin: 'testPin'
            })
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('error');
                    result.body.should.be.an.Object;
                    testCampaign = result.body
                    done()
                })
        })

    })


    describe('#--user ', function(){
        it('login, should return error null, object', function(done){
            this.timeout(5000)
            request(this.server)
                .post('/auth/login')
                .send({username: 'mocha-test-user', password: 'no password'})
                .expect(200)
                .end(function(err, result){
                    testUser = result.body
                    CookiesUser = result.headers['set-cookie'].pop().split(';')[0];
                    should.not.exist(err);
                    should.exist(result);
                    result.should.be.an.JSON
                    result.body.should.be.an.Object;
                    done()
                })
        })
        it('login, should return error null, object', function(done){
            before(function(done){
                this.browser.visit('/admin/login', done);
            })
        })


        it('auth-remote-create, should return err null', function(done){
            request(this.server)
                .post('/user/auth-remote')
                .send({username: 'mocha-test-user'})
                .expect(200, function(err, result){
                    should.not.exist(err);
//                    result.text.should.not.be.equal('error');
//                    result.should.be.an.JSON;
//                    result.body.should.be.an.Array;
                    done()
                })
        })
        it('auth-remote-find, should return err null', function(done){
            request(this.server)
                .post('/user/auth-remote')
                .send({username: 'mocha-test-user'})
                .expect(200, function(err, result){
                    should.not.exist(err);
//                    result.text.should.not.be.equal('error');
//                    result.should.be.an.JSON;
//                    result.body.should.be.an.Array;
                    done()
                })
        })

        it('find all, should return array users', function(done){
            request(this.server)
                .get('/user/all')
                .expect(200, function(err, result){
                    should.not.exist(err);
                    result.text.should.not.be.equal('error');
                    result.should.be.an.JSON;
                    result.body.should.be.an.Array;
                    done()
                })
        })

        it('find rank, should return object user', function(done){
            var req = request(this.server).get('/user')

            req.cookies = CookiesUser
            req.expect(200)
                .end(function(err, result){
//                    console.log('====', result.test, result.body)
                    should.not.exist(err);
                    result.should.be.an.JSON;
                    result.body.should.be.an.Object;
                    done()
                })
        })

    })



    describe('#--game', function(){

        it('create, should return object ', function(done){
            var req = request(this.server).post('/game')

            req.cookies=CookiesUser
            req.send({
                    score: 1,
                    time: 2,
                    result: [0, 1, 2, 3],
                    errorSymbol: [],
                    user_id: testUser._id,
                    campaign_id: testCampaign._id
                })
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('error');
                    result.body.should.be.an.Object;
                    testGame = result.body
                    done()
                })
        })

        it('find by id, should return object ', function(done){
            var req = request(this.server).get('/game/'+testGame._id)

            req.cookies=CookiesUser
            req.expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('err');
                    result.body.should.be.an.Object;
                    result.body.game.should.be.an.Object;
                    result.body.game.campaign.should.be.an.Object;
                    result.body.game.user.should.be.an.Object;
                    done()
                })
        })

        it('add user, should return object ', function(done){
            var req = request(this.server).put('/game')

            req.cookies=CookiesUser
            req.send({
                    campaign: testCampaign._id
                })
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    result.text.should.not.be.equal('error');
                    done()
                })
        })
    })

    describe('#--delete', function(){

        it('user, should return "ok" ', function(done){
            request(this.server)
                .delete('/admin/users/'+testUser._id)
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('error');
                    result.text.should.equal('ok');
                    done()
                })
        })

        it('game, should return "ok" ', function(done){
            request(this.server)
                .delete('/admin/games/'+testGame._id)
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('error');
                    result.text.should.equal('ok');
                    done()
                })
        })


        it('campaign, should return "ok" ', function(done){
            request(this.server)
                .delete('/admin/campaigns/'+testCampaign._id)
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('error');
                    result.text.should.equal('ok');
                    done()
                })
        })

        it('admin, should return "ok" ', function(done){
            request(this.server)
                .delete('/admin/user-delete/'+testAdmin._id)
                .expect(200)
                .end(function(err, result){
                    should.not.exist(err);
                    should.exist(result);
                    result.text.should.not.be.equal('error');
                    result.body.should.be.an.Array;
                    done()
                })
        })
    })


    after(function(done) {
        var self = this
        this.timeout(5000)
        request(this.server)
            .get('/admin/dropDatabase')
            .end(function(){
                self.server.close(done);
            })
//        this.browser.close()
    });
})
