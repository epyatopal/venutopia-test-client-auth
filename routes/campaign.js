var env = process.env.NODE_ENV || 'development'
    , config = require('../config/config')
    , user = require('../routes/user')
    , mongoose = require('mongoose')
    , Campaign = mongoose.model('Campaign')

exports.boot = function (app, passport, auth) {

    app.get('/', function(req, res) {
        Campaign.find({}, function(err, campaigns) {
            if (err || !campaigns.length) return res.send('err')
            res.send(campaigns)
        })
    })
    app.get('/:id', function(req, res) {

//        var pin = new RegExp('^' + req.params.id + '$', 'i')
        var pin = req.params.id
        console.log('========================+>>>>>>>>>>>>>>>>>>>> ', req.params, pin);
        Campaign.findOne({pin:pin, status:true}, function(err, campaign) {
            console.log('error ===>>>>>>>>>>>>>>>>>>> ', err, campaign);
            if (err || !campaign) return res.send('err')
            res.send(campaign)
        })
    })

    app.get('/create', function(req, res) {
        Campaign.create({
                tool:'Keyboard Wars'
                , title:'Roberts Test'
                , sponsor: 'SweetSmart'
                , status:'running'
                , image: 'images/sponsors/Zevia_PMS_Logo.jpg'
                , text: 'zero calories and naturally sweetened! #SweetSmart'
            },
            function (err, newCampaign) {
                res.send(200, newCampaign);
            });
    })
}