var FacebookStrategy = require('passport-facebook').Strategy
    , GoogleStrategy = require('passport-google-oauth').OAuth2Strategy
    , TwitterStrategy = require('passport-twitter').Strategy
    , LocalStrategy = require('passport-local').Strategy
    , env = process.env.NODE_ENV || 'development'
    , config = require('../config/config')
    , user = require('../routes/user')
    , mongoose = require('mongoose')
    , User = mongoose.model('User')
    , Campaign = mongoose.model('Campaign')
    , FB = require('fb')
    , request = require('request');


exports.boot = function (app, passport, auth) {

    app.get('/', function(req, res) {
        res.render('auth/login')
    })

//    app.post('/login', function(req, res){
//        console.log('11111 user user user user user user ======>>>>>>>>> ', req.session);
//        if ((!req.body.username)) {
//            return res.send(401)
//        }
//        user.findOrCreateMLBUser(req.body.username, function(err, user){
//            if (err) return res.send(401);
//            req.session.passport.user = user;
//            console.log('3333 user user user user user user ======>>>>>>>>> ', req.session);
//            res.send(user);
//        })
//    })


    app.post('/login/social-mlb', function(req, res){
        var user = JSON.parse(req.body.user);
        console.log('===============+>>>> ', req.session.user, user)
        if (req.session.user)
            return res.send(req.session.user);
        if (user.type == 'facebook') {
            FB.setAccessToken(user.token.accessToken);
            FB.api('/me', function(response) {
                console.log('========================>>>>>>>> ', response);
                    User.findOne({
                        $or: [
                            { 'social_id.facebook': response.id},
                            { email: response.email}
                        ]
                    }, function (err, user) {
                        if (user) {
                            // update needed to link to existing email user
                            user.social_id.facebook = response.id;
                            user.save();
                            req.session.user = user;
                            req.session.passport.user = user;
                            req.session.save();
                            return res.json(user);
                        } else {
                            User.create({
                                username: response.name,
                                email: response.email,
                                score: 0.00,
                                social_id: {facebook: response.id}
                            },
                            function (err, newUser) {
                                if (err) return
                                req.session.user = newUser;
                                req.session.passport.user = newUser;
                                req.session.save();
                                return res.json(newUser);
                            });
                        }
                    });
            });

        } else if (user.type == 'google') {
            request('https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=' + user.token.access_token, function(err, res, body) {
                if (err) {
                    console.log(err);
                    return
                }

                if (res.statusCode != 200) {
                    console.log('Invalid access token: ' + body);
                    return
                }
                else {
                    var me;
                    try { me = JSON.parse(body);}

                    catch (e) {
                        console.log('Unable to parse user data: ' + e.toString());
                        return
                    }

                    console.log('user profile:', me);
//                    User.findOne({
//                        $or: [
//                            { 'social_id.google': me.id},
//                            { email: me.email}
//                        ]
//                    }, function (err, user) {
//                        if (user) {
//                            // update needed to link to existing email user
//                            user.social_id.google = me.id;
//                            user.save();
//                            req.session.user = user;
//                            req.session.passport.user = user;
//                            req.session.save();
//                            return res.json(user);
//                        } else {
//                            User.create({
//                                    username: me.name,
//                                    email: me.email,
//                                    score: 0.00,
//                                    social_id: {google: me.id}
//                                },
//                                function (err, newUser) {
//                                    if (err) return
//                                    req.session.user = newUser;
//                                    req.session.passport.user = newUser;
//                                    req.session.save();
//                                    return res.json(newUser);
//                                });
//                        }
//                    });
                }
            });
        } else res.send(401);
    })

    app.get('/facebook', function (req, res) {
        passport.authenticate('facebook')(req, res)
    });

    app.get('/facebook/callback',
        passport.authenticate('facebook', { successRedirect: '/',
                failureRedirect: '/auth'}
        ));

    app.get('/google', function (req, res) {
        passport.authenticate('google')(req, res)
    });

    app.get('/google/callback',
        passport.authenticate('google', { successRedirect: '/',
                failureRedirect: '/auth'}
        ));

    app.get('/twitter', function (req, res) {
        passport.authenticate('twitter')(req, res)
    });

    app.get('/twitter/callback',
        passport.authenticate('twitter', { successRedirect: '/',
                failureRedirect: '/auth'}
        ));

    app.get('/logout', function(req, res){
        req.logOut();
        res.clearCookie("user");
        res.send(200)
        res.redirect('/auth');

    });

    app.get('/me', function(req, res){
        console.log('---------------_>>>>> ', req.session);
        res.send(req.session)

    });

    passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
            clientSecret: config.facebook.clientSecret,
            callbackURL: config.facebook.callbackURL,
            scope: 'email' //, user_birthday'
//            passReqToCallback: true
        },
        function (accessToken, refreshToken, profile, done) {
            user.findOrCreateFacebookUser(profile, done);
        }
    ));

    passport.use(new GoogleStrategy({
            clientID: config.google.clientID,
            clientSecret: config.google.clientSecret,
            callbackURL: config.google.callbackURL,
            scope: ['profile', 'email']
        },
        function (accessToken, refreshToken, profile, done) {
            user.findOrCreateGoogleUser(profile, done);
        }
    ));

    passport.use(new TwitterStrategy({
            consumerKey: config.twitter.consumerKey,
            consumerSecret: config.twitter.consumerSecret,
            callbackURL: config.twitter.callbackURL,
            scope: 'email'
        },
        function (accessToken, refreshToken, profile, done) {
            user.findOrCreateTwitterUser(profile, done);
        }
    ));

    passport.use(new LocalStrategy(
        function(username, password, done) {
            console.log('here!!!!!---------------->>>>>>>>', username, password)

//            User.findOrCreate({ username: username }, function (err, user) {

            user.findOrCreateMLBUser(username, function(err, user){
                if (err) { return done(err); }
                if (!user) { return done(null, false); }
//                if (!user.verifyPassword(password)) { return done(null, false); }
                return done(null, user);
            });
        }
    ));

    passport.serializeUser(function (user, done) {
        done(null, user);
    });

    passport.deserializeUser(function (user, done) {
        done(null, user);
    });

}