
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', {
      title: 'Venutopia Games',
      user : req.session.user // get the user out of session and pass to template
  });
};