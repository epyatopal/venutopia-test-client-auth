var env = process.env.NODE_ENV || 'development'
    , config = require('../config/config')
    , fs = require('fs')
    , request = require('request').defaults({ encoding: null })
    , mongoose = require('mongoose')
    , User = mongoose.model('User')
    , Admin = mongoose.model('Admin')
    , Campaign = mongoose.model('Campaign')
    , Game = mongoose.model('Game')
    , Rating = mongoose.model('Rating')
    , sha256 = require('sha256')
    , https = require('https')
    , async = require('async')
    , _ = require('underscore')

var adminauth = function(req, res, next){
    if (req.session && req.session.adminUser)
        next();
    else
        res.redirect('admin/login')
};

exports.boot = function (app, auth) {

    app.get('/', adminauth, function(req, res){
        res.render('admin/admin');
    });

    app.get('/login', function(req, res){
        res.render('admin/login')
    });

    app.post('/login', function(req, res){
        Admin.findOne({name: req.body.name, password: sha256(req.body.password)}, function(err, user){

//            if (req.body.username =='admin' && req.body.password == "admin" || user){
            if (user && !user.isInactive){
                req.session.adminUser = user
                app.emit('event:log_info', 'Admin '+JSON.stringify(req.session.adminUser) +" has been logged"+' ('+(req.headers.origin || req.headers.host)+')');
                res.cookie('user', JSON.stringify({
                    'name': user.name,
                    'role': user.role,
                    '_id': user._id
                }));
                res.send(200)
            } else {
                app.emit('event:log_info', 'Error admin login');
                res.send(user)
            }
        })

    });

    app.get('/logout', function(req, res){
        app.emit('event:log_info', 'Admin '+JSON.stringify(req.session.adminUser)+" logout"+' ('+(req.headers.origin || req.headers.host)+')');
        req.session.adminUser = false
        res.redirect('/admin/login')
    });

    app.get('/users', function(req, res) {
        User.find({}, null, {sort: {score: -1}}, function(err, users) {
            if (err || !users) return res.send('error')
            res.send(users)
        })
    })

    app.get('/ratings', function(req, res) {
        Rating.find({}, null, {sort: {createdAt: -1}}, function(err, ratings) {
            if (err || !ratings) return res.send('error')
            res.send(ratings)
        })
    })

    app.get('/games', function(req, res) {
        var populateQuery = ['user', 'campaign']
        Game.find({}).populate(populateQuery).exec(function(err, games) {
            if (err || !games) return res.send('error')
            res.send(games)
        });
    })

    app.get('/dashboard', function(req, res) {
        Campaign.find({}, function(err, campaigns) {
            if (err || !campaigns) return res.send('error')
            if(req.session.adminUser.role != 'Admin') {
                Admin.findById(req.session.adminUser._id, function(err, admin){
                    if (err || !admin) return res.send('error')
                    var _mas =[]
                    admin.campaign.forEach(function(campaign){
                        _mas.push(function(cb){
//                            var el = _.findWhere(campaigns, {_id: campaign})
                            var el = campaigns.filter(function(item){
                                if (item._id.toString() == campaign.toString()) return item
                            })
                            cb (err, el[0])
                        })
                    })
                    async.parallel(_mas, function(err, result){
                        return res.send(result)
                    })

                })
            }else{
                res.send(campaigns)
            }
        })
    })
    app.get('/campaigns', function(req, res) {
        Campaign.find({}, function(err, campaigns) {
            if (err || !campaigns) return res.send('error')
            if(req.session.adminUser.role != 'Admin') {
                Admin.findById(req.session.adminUser._id, function(err, admin){
                    if (err || !admin) return res.send('error')
                    var _mas =[]
                    admin.campaign.forEach(function(campaign){
                        _mas.push(function(cb){
//                            var el = _.findWhere(campaigns, {_id: campaign})
                            var el = campaigns.filter(function(item){
                                if (item._id.toString() == campaign.toString()) return item
                            })
                            cb (err, el[0])
                        })
                    })
                    async.parallel(_mas, function(err, result){
                        return res.send(result)
                    })

                })
            }else{
                res.send(campaigns)
            }
        })
    })

    app.delete('/users/:id', function(req, res) {
        User.findOne({_id: req.params.id }, function(err, users) {
            if (err || !users) return res.send('error')
        }).remove(function(err, result) {
                if (err || !result) return res.send('error')
                app.emit('event:log_info', 'User with id '+req.params.id+" has been deleted"+' ('+(req.headers.origin || req.headers.host)+')');
                res.send('ok')
            })
    })

    app.delete('/games/:id', function(req, res) {
        Game.findOne({_id: req.params.id }, function(err, games) {
            if (err || !games) return res.send('error')
        }).remove(function(err, result) {
                if (err || !result) return res.send('error')
                app.emit('event:log_info', 'Game with id '+req.params.id+" has been deleted"+' ('+(req.headers.origin || req.headers.host)+')');
                res.send('ok')
            })
    })

    app.delete('/campaigns/:id', function(req, res) {
        Campaign.findOne({_id: req.params.id }, function(err, campaign) {
            if (err || !campaign) return res.send('error')
        }).remove(function(err, result) {
                if (err || !result) return res.send('error')
                app.emit('event:log_info', 'Campaign with id '+req.params.id+" has been deleted"+' ('+(req.headers.origin || req.headers.host)+')');
                res.send('ok')
            })
    })
//    app.get('/sponsors/:id', function(req, res) {
//        Sponsor.findOne({_id: req.params.id }, function(err, sponsor) {
//            if (err || !sponsor) return res.send('error')
//            res.send('ok',sponsor)
//        })
//    })

    app.post('/campaigns/new', function(req, res) {
        if(req.session.adminUser=='admin') return res.send('error')
//        console.log('============>>>>>>>>>', req.session, req.cookies)

        Admin.findById(req.session.adminUser._id, function(err, admin){
            if (err || !admin) return res.send('error')
            Campaign.create({
                tool:req.body.tool
                , title:req.body.title
                , sponsor: req.body.sponsor
                , status:false
                , text: req.body.text
                , pin: req.body.pin
            }, function (err, newCampaign) {
                if (err || !newCampaign) return res.send('error')
                admin.campaign.push(newCampaign)
                admin.save()
                app.emit('event:log_info', 'Created new campaign '+JSON.stringify(newCampaign)+' ('+(req.headers.origin || req.headers.host)+')');
                res.send(newCampaign);
            });
        })

    })

    app.get('/campaigns/edit/:id',function(req,res){
        Campaign.findById(req.params.id,function(err,campaign){
            if (err || !campaign) return res.send('error')
            Game.find({campaign: campaign._id}, function(err, games){
                if (err) return res.send('error')
                res.send({campaign: campaign, games: games})
            }).populate('user')
        })
    })

    app.put('/campaigns/edit/:id',function(req,res){
        Campaign.findById( req.params.id, function(err,campaigns){
            campaigns.tool=req.body.tool||campaigns.tool
            campaigns.title=req.body.title||campaigns.title
            campaigns.sponsor=req.body.sponsor||campaigns.sponsor
            campaigns.text=req.body.text||campaigns.text
            campaigns.pin=req.body.pin||campaigns.pin
            campaigns.prize=req.body.prize||campaigns.prize
            campaigns.instructions=req.body.instructions||campaigns.instructions
            campaigns.code=req.body.code||campaigns.code

            if (req.body.file) {
                var s3FileName = '/' + campaigns._id + '.' + req.body.filetype

                https.get(req.body.file, function(data){
                    var headers = {
                        'Content-Length': data.headers['content-length']
                        , 'Content-Type': data.headers['content-type']
                        , 'x-amz-acl': 'public-read'
                    };
                    app.get('S3').putStream(data, s3FileName, headers, function(err, data){
                        if (err) res.send('error')
                        campaigns.image = data.req.url
                        campaigns.save(function(err,campaignsSave){
                            if (err) return res.send('error')
                            console.log('error db', err,campaignsSave)
                            app.emit('event:log_info', 'Campaign has been edited: '+JSON.stringify(campaignsSave));
                            return res.send(campaignsSave)
                        })
                    });
                });
            }else{
                campaigns.save(function(err,campaigns){
                    if (err) return res.send('error')
                    app.emit('event:log_info', 'Campaign has been edited: '+JSON.stringify(campaigns));
                    return res.send(campaigns)
                })
            }
        })
    })


    app.put('/stats/:id',function(req,res){

        Campaign.findOne({_id: req.params.id}, function(err, doc) {
            doc.status = !doc.status
            doc.save(function(){
                app.emit('event:log_info', 'Campaign has been edited: '+JSON.stringify(doc)+' ('+(req.headers.origin || req.headers.host)+')');
                res.send(doc);
            })
        })
//        Sponsor.findOne({_id: req.params.id}, function(err, sponsors) {
//            if (err || !sponsors) return res.send('error')
//        }).update({
//            status: req.body.status
//        },function(err){
//            if (err) return res.send('error')
//

    })

    app.get('/campaign/games/:id', function (req, res) {
        console.log('id', req.params.id)
        Game.find({campaign: req.params.id}, function (err, games) {
            if (err || !games) return res.send('err')
            console.log('find',games)
            res.send(games)
        })
    })

    app.get('/check/:name', function(req, res) {

        Campaign.findOne({pin: req.params.name}, function(err, campaign) {
            console.log('campaign',campaign)
            res.send(campaign)
        })
    })

    app.get('/dropDatabase', function(req, res) {
        if(process.env.NODE_ENV == "mocha"){
            mongoose.connection.db.dropDatabase(function(status){
                console.log('status', status)
                res.send(200)
            })
        }else{
            res.send('you not in test')
        }

    })

    // creating and modifying user admin
    app.post('/create-user', function(req, res) {
        Admin.create({
            name: req.body.name
            , password: sha256(req.body.password)
            , role: req.body.role
            , firstname: req.body.firstname
            , lastname: req.body.lastname
            , company: req.body.company
            , isInactive: req.body.isInactive

    }, function(err, user){
            if (err || !user) return res.send('error')
            app.emit('event:log_info', 'Action administrator: User with id '+user.id+" has been created"+' ('+(req.headers.origin || req.headers.host)+')');
            res.send(200)
        })
    })

    app.get('/load-users', function(req, res) {
        Admin.find({}, function(err, users){
            if (err || !users) return res.send('error')
            res.send(users)
        })
    })

    app.get('/find-changed-user/:id', function(req, res) {
        Admin.findById(req.params.id, function(err, user){
            if (err || !user) return res.send('error')
            res.send(user)
        })
    })

    app.put('/change-user', function(req, res) {
        Admin.findOne({name: req.body.name}, function(err, findUser){
            if (findUser && findUser._id != req.body._id) return res.send('errorName');
            Admin.findById(req.body._id, function(err, user){
                if (err || !user) return res.send('error');
                req.body.password = req.body.password ? sha256(req.body.password) : user.password;
                user.name= req.body.name;
                user.firstname = req.body.firstname;
                user.lastname = req.body.lastname;
                user.company = req.body.company;
                user.isInactive = req.body.isInactive;
                user.password = req.body.password;
                user.role = req.body.role;
                user.save();
                app.emit('event:log_info', 'Action administrator: User with id '+user.id+" has been changed"+' ('+(req.headers.origin || req.headers.host)+')');
                res.send(200);
            })
        })
    })

    app.delete('/user-delete/:id', function(req, res) {
        Admin.findOne({_id: req.params.id }, function(err, user) {
            if (err || !user) return res.send('error')
        }).remove(function(err, result) {
                if (err || !result) return res.send('error')
                app.emit('event:log_info', 'Action administrator: User with id '+req.params.id+" has been deleted"+' ('+(req.headers.origin || req.headers.host)+')');
                Admin.find({}, function(err, users){
                    if (err) return res.send('error')
                    res.send(users)
                })
            })
    })

}