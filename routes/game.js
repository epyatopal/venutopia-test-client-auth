var env = process.env.NODE_ENV || 'development'
    , config = require('../config/config')
    , mongoose = require('mongoose')
    , Game = mongoose.model('Game')
    , Campaign = mongoose.model('Campaign')
    , _ = require('underscore')


exports.boot = function (app, auth) {

    app.get('/', auth, function (req, res) {
        Game.find({}, function (err, game) {
            if (err || !game) return res.send('err')
            res.send(game)
        })
    })


    app.get('/:id', auth, function (req, res) {
        Game.findOne({_id: req.params.id}, function (err, game) {
            if (err || !game) return res.send('err')
//            Game.count({score: {$gt: game.score}, sponsor: game.sponsor._id, user: game.user._id}, function(err, count) {
//                res.send({game: game, count: count})
//            })
            Game.find({campaign: game.campaign._id, user: game.user._id})
                .sort ('-score time')
                .exec ( function(err, mas) {
                    var count = _.indexOf(mas, _.findWhere(mas, {score: game.score, time: game.time}))
                    res.send({game: game, count: count})
                })
        }).populate('user campaign')
    })


    app.get('/position/:id', auth, function (req, res) {
        Game.find({campaign: req.params.id}, function (err, games) {
            if (err) return res.send('err')
            res.send({games: games})
        })
    })

    app.get('/campaign/:id', auth, function (req, res) {
        Game.find({campaign: req.params.id}, function (err, games) {
            if (err) return res.send('err')
            res.send({games: games})
        }).populate('user campaign')
    })

    app.post('/', auth, function (req, res) {

        Game.create({
            score: req.body.score,
            time: req.body.time,
            result: req.body.result,
            errorSymbol: req.body.errorSymbol,
            user: req.body.user_id,
            campaign: req.body.campaign_id
        }, function (err, newgame) {
            if (err) return res.send('error')
            app.emit('event:log_info', 'Create new game: '+JSON.stringify(newgame)+' ('+(req.headers.origin || req.headers.host)+')');
            res.send(newgame)
        });
    })


    app.get('/check', auth, function (req, res) {
        Campaign.find({
            sponsor: req.body.campaign_id
        }, function (err, newgame) {
            if (err) return res.send('error')

            res.send(newgame)
        });
    })




    app.put('/', auth, function (req, res) {
        Campaign.update({_id: req.body.campaign},{$addToSet:
            { user: req.session.user._id}
        }, function (err, newgame) {
            app.emit('event:log_info', 'User '+JSON.stringify(req.session.user)+" has been added to campaign: "+req.body.campaign+' ('+(req.headers.origin || req.headers.host)+')');
            if (err) return res.send('error')
             res.send(200)
        });
    })
}