var mongoose = require('mongoose')
    , User = mongoose.model('User')
/*
 * GET users listing.
 */

exports.boot = function (app, passport, auth) {

    app.get('/all', function(req, res) {
        User.find({}, null, {sort: {score: -1}}, function(err, users) {
            if (err || !users) return res.send('error')
            res.send(users)
        })
    })


    //for test without user    
    // app.get('/', function(req, res) {

    //     User.findOne({}, function(err, user) {        
    //     // User.findOne({_id: req.session.user._id}, function(err, user) {
    //         res.send(user)
    //     })
    // })

    app.get('/', auth, function(req, res) {

        User.find({}, null, {sort: {score: -1}}, function(err, users) {
            users.forEach(function(el, index){
                if (el._id == req.session.user._id) {
                    req.session.user.rank = index+1
                    return res.jsonp(req.session.user)
                }
            })
        })

//        })
    })

    app.put('/:id', auth, function(req, res) {
        if(!(req.body.username&&req.body.username.length)) return res.send({err: 'Enter name please'})
        User.findOne({username: req.body.username}, function(err, user){
            if(err) return res.send({err: err})
            if(user && (user._id != req.params.id)) return res.send({err: 'This name already in use'})
            User.update({_id: req.params.id}, req.body, function(err, user) {
                if(err) return res.send({err: err})
                if(!user)return res.send({err: 'Your profile is not find'})
                req.session.passport.user.username = req.body.username
                app.emit('event:log_info', 'User with id '+req.session.user._id +" changed his name to "+req.body.username+' ('+(req.headers.origin || req.headers.host)+')');
                res.send({err: null})
            })
        })
    })

    app.put('/', auth, function(req, res) {
        User.findOne({_id: req.session.user._id}, function(err, user) {
            if (err || !user) return res.send('error')
            if (!req.body.score || (user.score && req.body.score <= user.score)) return res.send(user)
            user.score = req.body.score
            user.time = req.body.time || user.time
            user.error = req.body.error
            user.save(function(err, userNew){
                if (err) return res.send('save error')
                app.emit('event:log_info', 'User with id '+req.session.user._id +" changed. Now it looks like this: "+JSON.stringify(userNew)+' ('+(req.headers.origin || req.headers.host)+')');
                res.send(userNew)
            })
//            var rankNew = rank(user, req.query)
//            User.find({$where : "this.score >= " + req.query.score}, null, {sort: {score: -1}}, function(err, users){
//                console.log("$where : ", users)
//                if (users.length == 0) {
//                    user.rank = 1
//                } else if (users[0].score != req.query.score) {
//                    user.rank = (parseInt(users[0].rank) + 1)
//                }
//                user.score = req.query.score
//                user.time = req.query.time || user.time
//                user.error = req.query.error || user.error
////                user.rank = rankNew || user.rank
//                user.save(function(err, userNew){
//                    if (err) return res.send('save error')
////                    User.where('rank', id).update({$set: {foo: 'bar'}}, function (err, count) {});
//                    User.update({$where : "this.rank >= " + userNew.rank}, {$inc: {rank: 1}}, {multi: true}, function(err, userUdt){
//                        if (err) return console.log('update error', err)
//                        res.send(userNew)
//                    });
//
//                })
//            })

        })
    })

    app.post('/auth-remote', function(req, res){
//        var user = req.body.username
        User.findOne({username: req.body.username}, function (err, user) {
            if (user) {
                req.session.passport.user = user
                app.emit('event:log_info', 'User '+JSON.stringify(user)+' has been remote logged'+' ('+(req.headers.origin || req.headers.host)+')');
                res.send(200);
            } else {
                User.create({
                    username: req.body.username,
                    score: 0.00
                },
                function (err, newUser) {
                    if (err) return res.send(401);
                    newUser.save();
                    req.session.passport.user = newUser


                    app.emit('event:log_info', 'Create new remote user: '+JSON.stringify(newUser)+' ('+(req.headers.origin || req.headers.host)+')');
                    res.send(200);
                });
            }
        });
    })

    app.get('/logged-user', function(req, res){
        res.send(req.session);
    })
}

exports.findOrCreateFacebookUser = function (profile, done) {
    User.findOne({
        $or: [
//            { social_id: {facebook: profile.id} },
            { 'social_id.facebook': profile.id},
            { email: profile.emails[0].value }
        ]
    }, function (err, user) {
        if (user) {
            // update needed to link to existing email user
            user.social_id.facebook = profile.id;
            user.save();
            done(null, user);
        } else {
            User.create({
                    username: profile.displayName,
                    email: profile.emails[0].value,
                    score: 0.00,
                    social_id: {facebook: profile.id}
                },
                function (err, newUser) {
                    if (err) return
//                    req.session.user = newUser
                    done(null, newUser);
                });
        }
    });
};

exports.findOrCreateGoogleUser = function (profile, done) {
    User.findOne({
        $or: [
            { 'social_id.google': profile.id},
            { email: profile.emails[0].value }
        ]
    }, function (err, user) {
        if (user) {
            // update needed to link to existing email user
            user.social_id.google = profile.id;
            user.save();
            done(null, user);
        } else {
            User.create({
                    username: profile.displayName,
                    email: profile.emails[0].value,
                    score: 0.00,
                    social_id: {google: profile.id}
                },
                function (err, newUser) {
                    if (err) return;
                    done(null, newUser);
                });
        };
    });
};

exports.findOrCreateTwitterUser = function (profile, done) {
    User.findOne({
        'social_id.twitter': profile.id
    }, function (err, user) {
        if (user) {
            done(null, user);
        } else {
            User.create({
                    username: profile.displayName,
                    //email: profile.emails[0].value,       // Twitter API doesn't return email address
                    score: 0.00,
                    social_id: {twitter: profile.id}
                },
                function (err, newUser) {
                    if (err) return;
                    done(null, newUser);
                });
        };
    });
};

exports.findOrCreateMLBUser = function (username, done) {

    User.findOne({outer_id: username}, function (err, user) {
        console.log('user', user)
        if (user) {
            done(null, user);
        } else {
            User.create({
                    outer_id: username,
                    email: '',
                    score: 0.00
                },
                function (err, newUser) {
                    if (err) return
                    done(null, newUser);
                });
        }
    });
}
