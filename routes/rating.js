var env = process.env.NODE_ENV || 'development',
    config = require('../config/config'),
    user = require('../routes/user'),
    mongoose = require('mongoose'),
    Rating = mongoose.model('Rating');

exports.boot = function (app, passport, auth) {

    app.get('/', function(req, res) {
        Rating.find({}, function(err, ratings) {
            if (err || !ratings.length) return res.send('error')
            res.send(ratings)
        })
    });

    app.get('/:id', function(req, res) {

        Rating.findOne({_id: req.params.id, status:true}, function(err, rating) {
            if (err || !rating) return res.send('error')
            res.send(rating)
        })
    });

    app.post('/', auth, function (req, res) {
        Rating.create({
            user: req.body.user_id,
            name: req.body.name,
            email: req.body.email,
            value: req.body.value,
            comments: req.body.comments
        }, function (err, newrating) {
            if (err) return res.send('error')
            app.emit('event:log_info', 'Create new rating: '+JSON.stringify(newrating)+' ('+(req.headers.origin || req.headers.host)+')');
            res.send(newrating);
        });
    })
}