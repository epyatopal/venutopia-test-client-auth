var winston = require('winston');
var logger = new (winston.Logger)({
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({ filename: 'logs.log' })
    ]
});

// Loggly config
var loggly = require('loggly');
var env    = process.env.NODE_ENV || 'development';
var client = loggly.createClient({
    token: "280ca209-6361-4506-a1d7-455196a99b96",
    subdomain: "c1oudbase",
    tags: ['NodeJS','venutopia-server-' + [env]],
    json:true
});

module.exports =  function(app){

    app.on('event:log_info', function(data){
        logger.log('info', data);
        client.log(data);
    });
//
//    app.on('event:user_change', function(data){
//        logger.log('info', data);
//    });
//
//    app.on('event:create_remote_user', function(data){
//        logger.log('info', data);
//    });

}