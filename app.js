
/**
 * Module dependencies.
 */


var express = require('express')
    , knox = require('knox')
    , routes = require('./routes')
    , db = require('./db/db')
    , user = require('./routes/user')
    , campaign = require('./routes/campaign')
    , game = require('./routes/game')
    , http = require('http')
    , path = require('path')
//    , env = process.env.NODE_ENV || 'development'
    , config = require('./config/config.js')
//    , config = require('./config/config.js')['development']
    , NM = require('express-namespace')
    , passport = require('passport')
    , morgan = require('morgan')
    , bodyParser     = require('body-parser')
    , methodOverride = require('method-override')
    , favicon = require('static-favicon')
    , cookieParser = require('cookie-parser')
    , session = require('express-session')
    , errorHandler = require('errorhandler')


var app = express();
logger = require ('./logger/logger.js')(app)

//CORS middleware
var allowCrossDomain = function(req, res, next) {
    res.header('Access-Control-Allow-Origin', config.remote_url);
    res.header('Access-Control-Allow-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
    res.header('Access-Control-Allow-Headers', 'Content-Type, Cookie');
    res.header("Access-Control-Allow-Credentials", "true");
    next();
}

// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(morgan('dev'));
//app.use(express.json());
app.use(bodyParser());
//app.use(express.urlencoded());
app.use(allowCrossDomain);
app.use(methodOverride());
app.use(cookieParser());
app.use(session({ secret: 'venutopiasecret' }));
app.use(passport.initialize());
app.use(passport.session());
//app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

var auth = function (req, res, next) {
    if (!req.isAuthenticated())
        res.redirect('/auth')
    else
        next();
};

// development only
if ('development' == app.get('env')) {
  app.use(errorHandler());
}

app.namespace('/auth', require('./routes/auth')
    .boot.bind(this, app, passport, auth));

app.namespace('/campaign', require('./routes/campaign')
    .boot.bind(this, app, passport, auth));

app.namespace('/user', require('./routes/user')
    .boot.bind(this, app, passport, auth));

app.namespace('/game', require('./routes/game')
    .boot.bind(this, app, auth));

app.namespace('/rating', require('./routes/rating')
    .boot.bind(this, app, passport, auth));

app.namespace('/admin', require('./routes/admin')
    .boot.bind(this, app));


app.get('/', auth, routes.index);

app.get('/test', function(req, res){
    res.render('test', { title: 'Venutopia Test Page' });
});

//app.get('/users', user.list);

// Health check per MLB requirement
app.get('/hcheck', function(req, res) {
    res.send('hcheck=ok');
    req.app.emit('event:log_info', 'Ran health check');
});

// Error message from auth check
app.get('/auth_error', function(req, res) {
    res.send('Auth error happened');
    req.app.emit('event:log_info', 'Auth error happened');
});

app.set('S3', knox.createClient({
    key: config.settings.accessKeyId
    , secret: config.settings.secretAccessKey
    , bucket: config.settings.bucket
}))

module.exports = app;
if (!module.parent) {
    http.createServer(app).listen(app.get('port'), function(){
        console.log('Express server listening on port ' + app.get('port'));
    });
}
