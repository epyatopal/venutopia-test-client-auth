var mongoose = require('mongoose')
    , timestamps = require('mongoose-timestamp');


var AdminSchema = new mongoose.Schema({
    name: { type: String }
    ,firstname: { type: String }
    ,lastname: { type: String }
    ,company: { type: String }
    ,password: { type: String }
    ,role: { type: String, default: 'User'}
    ,campaign: [{ type: mongoose.Schema.Types.ObjectId, ref: 'campaign' }]
    ,isInactive: { type: Boolean}
});

AdminSchema.plugin(timestamps);
mongoose.model('Admin', AdminSchema);