var mongoose = require('mongoose')
    , timestamps = require('mongoose-timestamp');


var GameSchema = new mongoose.Schema({
    score: { type: Number }
    , time: { type: Number }
    , result: { type: Array }
    , errorSymbol: { type: Array }
    , user: { type: mongoose.Schema.Types.ObjectId, ref: 'User' }
    , campaign: { type: mongoose.Schema.Types.ObjectId, ref: 'Campaign' }
});

GameSchema.plugin(timestamps);
mongoose.model('Game', GameSchema);
/**/