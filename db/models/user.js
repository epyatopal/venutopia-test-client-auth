var mongoose = require('mongoose')
    , timestamps = require('mongoose-timestamp');


var UserSchema = new mongoose.Schema({
    username: { type: String }
    , password: { type: String }
    , email: { type: String }
    , social_id: {
        facebook: { type: String }
        , google: { type: String }
        , twitter: { type: String }
    }
    , outer_id: { type: String }
    , score: { type: Number }
    , time: { type: Number }
    , error: { type: Number }
    , rank: { type: Number }
});

UserSchema.plugin(timestamps);
mongoose.model('User', UserSchema);
/**/
module.exports.User = mongoose.model('User', UserSchema);