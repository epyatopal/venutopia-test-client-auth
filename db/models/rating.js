var mongoose = require('mongoose')
    , timestamps = require('mongoose-timestamp');


var RatingSchema = new mongoose.Schema({
    user: [{ type: mongoose.Schema.Types.ObjectId, ref: 'user' }],
    name: { type: String },
    email: { type: String },
    value: { type: Number },
    comments: { type: String },
    isCompleted: { type: Boolean, default: false}
});

RatingSchema.plugin(timestamps);
mongoose.model('Rating', RatingSchema);