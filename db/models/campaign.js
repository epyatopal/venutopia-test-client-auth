var mongoose = require('mongoose')
    , timestamps = require('mongoose-timestamp');


var CampaignSchema = new mongoose.Schema({
    tool: { type: String }
    ,title: { type: String }
    ,sponsor: { type: String}
    ,status:{ type:Boolean, default: false}
    ,image: { type: String }
    ,text: { type: String }
    ,pin: { type: String }
    ,prize:{type:String}
    ,instructions:{type:String}
    ,code:{type:String}
//    ,imageUrl:{type:String}
    ,user: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }]
});

CampaignSchema.plugin(timestamps);
mongoose.model('Campaign', CampaignSchema);
/**/
/**/