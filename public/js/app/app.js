'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('app', [
    'ngRoute',
    'controllers',
    'services',
    'directives']);

app.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
    $routeProvider
        .when('/play/', {
            templateUrl: 'partials/play.html',
            controller: 'PlayCtrl'
        })
        .when('/play/:id',{
            templateUrl: 'partials/play.html',
            controller: 'PlayCtrl'
        })
        .when('/gamepin', {
            templateUrl: 'partials/gamepin.html',
            controller: 'GamePinCtrl'
        })
        .when('/result/:id', {
            templateUrl: 'partials/result.html',
            controller: 'ResultCtrl'
        })
        .when('/leaderboard/:id', {
            templateUrl: 'partials/leaderboard.html',
            controller: 'LeaderboardCtrl'
        })
        .when('/contactus', {
            templateUrl: 'partials/contactus.html'
        })
        .when('/funfacts', {
            templateUrl: 'partials/funfacts.html'
        })
        .when('/privacy', {
            templateUrl: 'partials/privacy.html'
        })
        .when('/terms', {
            templateUrl: 'partials/terms.html'
        })
        .when('/rate', {
            templateUrl: 'partials/rate.html',
            controller: 'RatingCtrl'
        })
        .when('/myprofile', {
            templateUrl: '/partials/myprofile.html'
        })
        .otherwise({
            redirectTo: '/gamepin'
        });
//        $locationProvider.html5Mode(true);
}]);

//app.run(function($location) {
//    $location.search({loaded: 'true'});
//});