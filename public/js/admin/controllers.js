'use strict';

/* Controllers */

var appControllers = angular.module('controllers', []);

appControllers.controller('usersCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'users'
        $scope.load()
    }
]);

appControllers.controller('createChangeUserCtrl', ['$scope', '$rootScope', '$http', '$location', '$timeout', 'API',
    function ($scope, $rootScope, $http, $location, $timeout, API) {
        var _mas = $location.$$url.split('/')
        $rootScope.url =  _mas[_mas.length-1]
        $rootScope.span = 'Create or change users'
        if ($rootScope.url == 'create-user')  $rootScope.span = 'Create user'
        $scope.newUser ={}
        $scope.newUser.role = 'User'
        $scope.adminListUsers =[]

        $scope.init = function(){
            API.request('admin/load-users').success(function (data, status, headers, config) {
                $scope.adminListUsers = data
            }).error(function (data, status, headers, config) {
                });
        }
        $scope.init()

        $scope.onUserDelete = function (id) {
//            $('#deldelModal').modal('hide');
            API.request('admin/user-delete/' + id, null, null, 'DELETE').success(function (data, status, headers, config) {
                $scope.adminListUsers = data
            }).error(function (data, status, headers, config) {
                });

        }

        $scope.createUser = function(){
            $timeout(function () {
                if (!$('#userForm input').hasClass('error')) {

                    var _el = _.findWhere($scope.adminListUsers, {name: $scope.newUser.name})
                    if (_el) {
                        $scope.errorName = 'This email already exists'
                        return
                    }
                    if ($scope.newUser.password != $scope.newUser.rePassword) {
                        $scope.error = 'Passwords are not equal, try again'
                        $scope.newUser.rePassword = ''
                        return
                    }
                    API.request('admin/create-user', null, $scope.newUser, 'POST').success(function (data, status, headers, config) {
                        if (data == 'error') return
                        $location.url('create-change-user')
                    }).error(function (data, status, headers, config) {
                        });
                }
            }, 100)
        }
    }
]);

appControllers.controller('changeUserCtrl', ['$scope', '$rootScope', '$http', '$location', '$timeout', 'API',
    function ($scope, $rootScope, $http, $location, $timeout, API) {
        $rootScope.url =  'change-user'
        $rootScope.span = 'Change user'
        $scope.changeUser={}

        var _mas = $location.$$url.split('/')
            , _id = _mas[_mas.length-1]
        $scope.init =function(){
            API.request('admin/find-changed-user/'+ _id).success(function (data, status, headers, config) {
                if (data == 'error') return
                $scope.changeUser = data
                $scope.changeUser.password = ''
            }).error(function (data, status, headers, config) {
                });
        }
        $scope.init()

        $scope.onChangeUser = function(){

            if ($scope.changeUser.password && $scope.changeUser.password != $scope.changeUser.rePassword) {
                $scope.error = 'Passwords are not equal, try again'
                $scope.changeUser.rePassword = ''
                return
            }
            API.request('admin/change-user', null, $scope.changeUser, "PUT").success(function (data, status, headers, config) {
                if (data == 'errorName') {
                    $scope.errorName = 'This email already exists'
                    return
                }
                if (data == 'error') return
                $location.url('create-change-user')
            }).error(function (data, status, headers, config) {
                });
        }
    }])

appControllers.controller('dashboardCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'dashboard'
        $rootScope.span = 'Dashboard'

        $scope.load(function () {
            $rootScope.items.forEach(function (el, idx) {
                var month = new Date(el.createdAt).getMonth() + 1
                    , date = new Date(el.createdAt).getDate()
                    , year = new Date(el.createdAt).getFullYear()
                if (month < 10) month = '0' + month
                if (date < 10) date = '0' + date
                el.date = month + '/' + date + '/' + year
            })

        })
    }
]);

appControllers.controller('campaignsCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'campaigns'
        $rootScope.span = 'Campaigns'

        $scope.load(function () {
            $rootScope.items.forEach(function (el, idx) {
                var month = new Date(el.createdAt).getMonth() + 1
                    , date = new Date(el.createdAt).getDate()
                    , year = new Date(el.createdAt).getFullYear()
                if (month < 10) month = '0' + month
                if (date < 10) date = '0' + date
                el.date = month + '/' + date + '/' + year
            })

        })
        console.log($location.url())
//        $rootScope.items.forEach(function(){
//            $rootScope.item.date= new Date($rootScope.item.createdAt())
//        })
//
    }
]);


appControllers.controller('gamesCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'games'
        $scope.load()
    }
]);

appControllers.controller('sponsorsCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'sponsors'
        $scope.load()
    }
]);

appControllers.controller('campaignsNewCtrl', ['$scope', '$rootScope', '$http', '$location', '$timeout', '$routeParams', 'API',
    function ($scope, $rootScope, $http, $location, $timeout, $routeParams, API) {
        $rootScope.url = 'campaigns/new'
        // $rootScope.span = 'New Keyboard Wars Campaign'

        $scope.campaign = {}
        $scope.campaignsName = $routeParams.name


        switch ($scope.campaignsName) {
            case 'keyboardwars':
                $rootScope.span = 'New Keyboard Wars Campaign'
                $scope.campaign.tool = 'Keyboard Wars'
        }
        $scope.create = function () {
            $timeout(function () {
                if (!$('#newGame input').hasClass('error')) {

                    $scope.checkPin($scope.campaign.pin, function(data){
                        if (data) return $scope.error = true
                        $scope.add($scope.campaign, function (newCampaign) {
                            $location.url($routeParams.name + '/campaigns/editImage/' + newCampaign._id)
                        })
                    })
                }
            }, 100)

        }
    }
]);

appControllers.controller('campaignsEditCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams', '$timeout', 'API',
    function ($scope, $rootScope, $http, $location, $routeParams, $timeout, API) {
        $scope.campaignsId = $routeParams.id
        $scope.campaignsName = $routeParams.name
        $rootScope.url = 'campaigns/edit/' + $scope.campaignsId

        $scope.campaign = {}
        // $rootScope.switchName()

        //$scope.load()
        $scope.getInfo($rootScope.url, function (data) {
            $scope.campaign = data.campaign

            switch ($scope.campaignsName) {
                case 'keyboardwars':
                    $rootScope.span = 'Edit Keyboard Wars Campaign'
                    $scope.campaign.tool = 'Keyboard Wars'
            }
        });
        $scope.create = function () {
            $timeout(function () {
                if (!$('#newGame input').hasClass('error')) {

                    $scope.checkPin($scope.campaign.pin, function(data){
                        if (data && data._id != $scope.campaign._id) return $scope.error = true
                        API.request('admin/campaigns/edit/' + $routeParams.id, null, $scope.campaign, 'PUT').success(function (data, status, headers, config) {
                            if (data == 'error') return
                            //  console.log('location')
                            $location.url($routeParams.name + '/campaigns/editImage/' + $scope.campaignsId)


                        }).error(function (data, status, headers, config) {
                        });
                    })
                }
            }, 100)

        }


    }
]);

appControllers.controller('campaignsEditImageCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams', 'API',
    function ($scope, $rootScope, $http, $location, $routeParams, API) {
        $rootScope.url = 'campaigns/editImage'
        $scope.campaignsName = $routeParams.name
        $scope.campaignsId = $routeParams.id
        $scope.thumbpick = false

        // load existing campaign
        $scope.campaign = {}
        $scope.getInfo('campaigns/edit/' + $scope.campaignsId, function (data) {
            $scope.campaign = data.campaign
        });


        $scope.setupCampaignPic = function (t) {
            var e = t;
            filepicker.convert(e, {height: 150, fit: "scale"}, function (e) {
//                $scope.fp_url = e.url
//                $scope.pic = e.key
//                $scope.pic_preview = e.url
                $scope.thumbpick = true
//                $scope.setupCampaignThumb(e)
                $("input[name='campaign[fp_url]']").val(e.url),
                    $("#campaign_pic").val(t.key),
                    $("#campaign_pic_preview").attr("src", e.url).show(),
                    $(".thumbpick").show(),
                    $scope.setupCampaignThumb(e)
            })
        }
        $scope.setupCampaignThumb = function (t) {
            $scope.jcrop_target = t.url
            $scope.preview = t.url
            $scope.$apply()
            $("#jcrop_target").attr("src", t.url).show(),
                $("#preview").attr("src", t.url).show();
            $scope.imageUrl = t.url
            $scope.imageType = t.filename.split('.').pop()

            var e = imagesLoaded("#jcrop_target");
            e.on("done", function(){
                arguments;
            })
        };

        $scope.saveImage = function(){
            var data = {file: $scope.imageUrl, filetype: $scope.imageType}
            API.request('admin/campaigns/edit/' + $routeParams.id, null, data, 'PUT').success(function (data, status, headers, config) {
                if (data == 'error') return
                //  console.log('location')
                $rootScope.goTo('/dashboard');


            }).error(function (data, status, headers, config) {
            });

        }

    }
]);
appControllers.controller('campaignsPrizesCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams', 'API',
    function ($scope, $rootScope, $http, $location, $routeParams, API) {
        $scope.campaignsId = $routeParams.id
        $rootScope.url = 'campaigns-prizes'

        // load existing campaign
        $scope.campaign = {}
        $scope.getInfo('campaigns/edit/' + $scope.campaignsId, function (data) {
            $scope.campaign = data.campaign
        });

        $scope.create = function () {
            API.request('admin/campaigns/edit/' + $routeParams.id, null, $scope.campaign, 'PUT').success(function (data, status, headers, config) {

                if (data == 'error') return
                $rootScope.goTo('/dashboard')


            }).error(function (data, status, headers, config) {
            });

        }

        //  $scope.load()
    }
]);
appControllers.controller('statsCtrl', ['$scope', '$rootScope', '$http', '$location', '$routeParams', 'API',
    function ($scope, $rootScope, $http, $location, $routeParams, API) {

        $rootScope.url = 'stats'
        $scope.campaignsName = $routeParams.name
        $scope.campaignsId = $routeParams.id

        // $scope.campaign = {}
        $scope.getInfo('campaigns/edit/' + $scope.campaignsId, function (data) {

            var sortGames = _.toArray(_.groupBy(data.games, function(game){
                return game.user._id
            }))
            $scope.users = []
            sortGames.forEach(function(games){
                var user = {}
                user.name = games[0].user.username
                user.score = _.max(games, function(game){
                    return game.score
                }).score
                user.submissions = games.length
                $scope.users.push(user)
            })
            $scope.users.sort(function(user1, user2){
                return user2.score - user1.score
            })
//
//            data.games.forEach(function(){
//
//            })

            console.log('$scope.users', $scope.users)


            $scope.campaign = data.campaign
            $rootScope.span = $scope.campaign.title
            $scope.players = data.campaign.user.length
            API.request('/admin/campaign/games/' + $routeParams.id).success(function (data1, status, headers, config) {
                if (data1 == 'error') return

                $scope.submissions = data1.length
                $scope.submissionsArr = data1
                $scope.avg = $scope.submissions / $scope.players || 0
                $scope.graficDate()
            }).error(function (data, status, headers, config) {
            });
            console.log('route', $routeParams.id)
        })




        $scope.renderCode = function () {
            var text = $location.host() + ':3000:/#/play/' + $routeParams.id

            var el =  $('#qrcodeholder')
            el.qrcode({
                text: text,
                render: "canvas",  // 'canvas' or 'table'. Default value is 'canvas'
                background: "#ffffff",
                foreground: "#000000",
                width: 90,
                height: 90
            });
        }
        $scope.renderCode()
        $scope.graficDate = function(){
//            API.request('/admin/campaign/games/' + $routeParams.id).success(function (data, status, headers, config) {
//                if (data == 'error') return
//
//                $scope.date = data.createdAt
//                $scope.avg = $scope.submissions / $scope.players
//            }).error(function (data, status, headers, config) {
//            });
            $scope.dateStart =  new Date($scope.campaign.createdAt) - 0
            $scope.dateEnd =  new Date() - 0

            $scope.dateArray = [];

            while($scope.dateStart < $scope.dateEnd) {
                var todayDate = new Date($scope.dateStart).getDate()
                var todayMonth = new Date($scope.dateStart).getMonth()
                var todayYear = new Date($scope.dateStart).getFullYear()
                var count = 0
                $scope.submissionsArr.forEach(function(el, idx) {
                    var gameDate = new Date(el.createdAt).getDate()
                    var gameMonth = new Date(el.createdAt).getMonth()
                    var gameYear = new Date(el.createdAt).getFullYear()
                    if (todayDate == gameDate && todayMonth == gameMonth && todayYear == gameYear) {
                        count++
                    }
                })
                if (todayDate<10) todayDate = '0'+todayDate
                todayMonth += 1
                if (todayMonth<10) {
                    todayMonth = '0'+todayMonth
                }
                var objGr = {
                    Day : todayYear + '-' + (todayMonth) + '-' + todayDate
                    , Value: count
                }
                $scope.dateArray.push(objGr);
                $scope.dateStart = new Date($scope.dateStart).setDate(todayDate+1)
            }
            new Morris.Line({
                // ID of the element in which to draw the chart.
                element: 'myfirstchart',
                // Chart data records -- each entry in this array corresponds to a point on
                // the chart.
                data: $scope.dateArray,
                // The name of the data record attribute that contains x-values.
                xkey: ['Day'],
                // A list of names of data record attributes that contain y-values.
                ykeys: ['Value'],
                // Labels for the ykeys -- will be displayed when you hover over the
                // chart.
                labels: ['Value']
            });
        };


        // $scope.start=true;
        //  $scope.load()
        $scope.changeStatus = function () {
            console.log('$scope.start', $scope.campaign.status)
            API.changeCampaignStatus($routeParams.id, {status: $scope.campaign.status}, function(data){
                console.log('data', data)
                $scope.campaign = data
            })
        }
    }
]);

appControllers.controller('ratingsCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'ratings';
        $rootScope.span = 'Ratings';

        API.request('rating')
            .success(function (data, status, headers, config) {
                if (data == 'error') return;
                $rootScope.ratings = data;
                $rootScope.ratings.forEach(function (el, idx) {
                    var month = new Date(el.createdAt).getMonth() + 1
                        , date = new Date(el.createdAt).getDate()
                        , year = new Date(el.createdAt).getFullYear()
                    if (month < 10) month = '0' + month
                    if (date < 10) date = '0' + date
                    el.date = month + '/' + date + '/' + year
                })
            })
            .error(function (data, status, headers, config) {
            });
    }
]);

appControllers.controller('sponsorsCtrl', ['$scope', '$rootScope', '$http', '$location', 'API',
    function ($scope, $rootScope, $http, $location, API) {
        $rootScope.url = 'sponsors'
        $scope.load()
    }
]);

appControllers.controller('menuCtrl', ['$scope', '$rootScope', '$location', 'API',
    function ($scope, $rootScope, $location, API) {
        $rootScope.goTo = function (path) {
            $location.url(path)

        }
    }
]);

appControllers.controller('commonCtrl', ['$scope', '$rootScope', '$location', '$timeout', 'API', '$cookieStore',
    function ($scope, $rootScope, $location, $timeout, API, $cookieStore) {
        $scope.admin = $cookieStore.get('user')
        $scope.load = function (callback) {
            API.request('admin/' + $rootScope.url).success(function (data, status, headers, config) {
                if (data == 'error') return
                $rootScope.items = data
                if (callback) callback()
            }).error(function (data, status, headers, config) {
            });
        }

        $scope.add = function (data, callback) {
            API.request('admin/' + $rootScope.url, null, data, 'POST').success(function (data, status, headers, config) {
                if (data == 'error') return
                if (callback) callback(data)
            }).error(function (data, status, headers, config) {
            });
        }

        $scope.showConfirmForDel = function (item) {
            $('#deldelModal').modal('show');
            $scope.itemForDel = item;
        }

        $scope.onDelete = function () {
            $('#deldelModal').modal('hide');
            API.request('admin/' + $rootScope.url + '/' + $scope.itemForDel._id, null, null, 'DELETE').success(function (data, status, headers, config) {
                $scope.load()
            }).error(function (data, status, headers, config) {
            });

        }
        $rootScope.getInfo = function (url, callback) {
            API.request('admin/' + url).success(function (data, status, headers, config) {
                if (data == 'error') return
                if (callback) callback(data)

            }).error(function (data, status, headers, config) {
            });
        }

        $scope.checkPin = function(pin, cb){
            API.request('admin/check/' +  pin).success(function (data) {
                cb(data)
            })
        }

        $scope.onDelCampaign = function(id){
            API.request('admin/campaigns/' + id, null, null, 'DELETE').success(function (data, status, headers, config) {
                if (data == 'error') return
                var p = _.findWhere($rootScope.items, {_id: id})
                $rootScope.items = _.difference($rootScope.items, p)

            }).error(function (data, status, headers, config) {
                });
        }

    }
]);


