
var app = angular.module('loginAdmin', [])

app.controller('commonCtrl', ['$scope', '$rootScope', '$http', '$location',
    function ($scope, $rootScope, $http, $location) {

        angular.element(document).keyup(function (event) {
            if (event.keyCode == 13) {
                $scope.onLogin();
            }
        });
        $scope.user={}
        $scope.onLogin = function(){
            if (!$scope.user.name) return $scope.errorName = 'This is a required field.'
            if (!$scope.user.password) return $scope.errorPassword = 'This is a required field.'
//            return {
//                checkPass: function (name, pass, callback) {

                    $http({
                        method: 'POST',
                        url: '/admin/login',
                        data: $scope.user
                    }).success(function (res) {
                            if (!res) return $scope.error = 'Invalid username or password.'
                            if (res.isInactive) return $scope.error = 'Your account is not active, please contact support@venutopia.com.'

                            //function redirect on admin page
                            var changeLocation = function(url, forceReload) {
                                $scope = $scope || angular.element(document).scope();
                                if(forceReload || $scope.$$phase) {
                                    window.location = url;
                                }
                                else {
                                    //only use this if you want to replace the history stack
                                    //$location.path(url).replace();

                                    //this this if you want to change the URL and add it to the history stack
                                    $location.path(url);
                                    $scope.$apply();
                                }
                            };
                            changeLocation('/admin')

                        }).error(function (err) {
                            console.log("Server error on login, please contact support@venutopia.com.", err)
                        });
//                }
//            }
        }

    }
]);