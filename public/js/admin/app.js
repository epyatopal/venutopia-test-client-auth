'use strict';

/* App Module */

var app;
app = angular.module('adminApp', [
    'ngRoute',
    'ngCookies',
    'controllers',
    'services',
    'directives'
]);


app.value('UserRoles', {
    admin : 'Admin'
    , user : 'User'
});

app.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
            .when('/users', {
                templateUrl: '/partials/admin/admin.html',
                controller: 'usersCtrl'
            })
            .when('/dashboard', {
                templateUrl: '/partials/admin/dashboard.html',
                controller: 'dashboardCtrl'
            })
            .when('/campaigns', {
                templateUrl: '/partials/admin/campaigns.html',
                controller: 'campaignsCtrl'
            })
             .when('/create-change-user', {
                templateUrl: '/partials/admin/create-change-user.html',
                controller: 'createChangeUserCtrl',
                access: 'Admin'
            })
             .when('/create-user', {
                templateUrl: '/partials/admin/create-user.html',
                controller: 'createChangeUserCtrl'
            })
             .when('/change-user/:id', {
                templateUrl: '/partials/admin/change-user.html',
                controller: 'changeUserCtrl'
            })
            .when('/games', {
                templateUrl: '/partials/admin/games.html',
                controller: 'gamesCtrl'
            })
            .when('/sponsors', {
                templateUrl: '/partials/admin/sponsors.html',
                controller: 'sponsorsCtrl'
            })
            .when('/:name/campaigns/new', {
                templateUrl: '/partials/admin/campaigns-new.html',
                controller: 'campaignsNewCtrl'
            })
            .when('/:name/campaigns/edit/:id', {
                templateUrl: '/partials/admin/campaigns-new.html',
                controller: 'campaignsEditCtrl'
            })
            .when('/:name/campaigns/editImage/:id', {
                templateUrl: '/partials/admin/campaigns-image.html',
                controller: 'campaignsEditImageCtrl'
            })
            .when('/:name/campaigns/prizes/:id', {
                templateUrl: '/partials/admin/campaigns-prizes.html',
                controller: 'campaignsPrizesCtrl'
            })
            .when('/stats/:id', {
                templateUrl: '/partials/admin/stats.html',
                controller: 'statsCtrl'
            })
            .when('/ratings', {
                templateUrl: '/partials/admin/ratings.html',
                controller: 'ratingsCtrl'
            })


            .otherwise({
                redirectTo: '/dashboard'
            });
    }]);

app.run(['$rootScope', '$location', 'Auth',function ($rootScope, $location, Auth) {
    $rootScope.$on("$routeChangeStart", function (event, next, current) {
        next && console.log(next.access)
        if (next.access) {
            var _el = Auth.checkRole(next.access)
            if (_el != true) {
                $location.path('/change-user/'+_el);
            }
        }
    });
}])