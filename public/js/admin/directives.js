var app = angular.module('directives', []);

app.directive("filepicker", function(){
    return {
        scope: {
            callback: '&',
            'pickerclass': '@'
        },
        transclude: true,
        restrict: "A",
        template: "<a href='javascript:;' class='{{pickerclass}}' ng-click='pickFiles()' ng-transclude ></a>",
        link: function(scope, element, attrs) {
            scope.pickFiles = function () {
                filepicker.setKey('AA5CHBPwQ8O6iqmHzjYAwz');
                filepicker.pick({
                        mimetypes: ['image/*', 'text/plain'],
                        container: 'modal'
//                        services:['COMPUTER', 'FACEBOOK', 'GMAIL']
                    },
                    function(InkBlob){
                        console.log(JSON.stringify(InkBlob));
                        scope.$apply(function(){
                        scope.callback({file:InkBlob});
                    });
                    },
                    function(FPError){
                        console.log(FPError.toString());
                    }
                );
//                var picker_options = {
//                    container: 'modal',
//                    mimetypes: 'image/*',//attrs.mimetypes ? eval(attrs.mimetypes) : ['*'],
//                    multiple: attrs.multiple ? eval(attrs.multiple) : false
//                };
//
//                var path = attrs.path ? attrs.path : '/uploads/',
//                    container = attrs.container ? attrs.container : 'documents.e-freightliner.com';
//
//                var store_options = {
//                    location: 'S3',
//                    path: path,
//                    container: container
//                };
////                filepicker.setKey('AA5CHBPwQ8O6iqmHzjYAwz');
//                filepicker.pickAndStore(picker_options, store_options, function (fpfiles) {
//                    scope.$apply(function(){
//                        scope.callback({file:fpfiles});
//                    });
//                });
            };
        }
    };
});
