var appServices = angular.module('services', []);

appServices.factory('API', ['$http', '$rootScope',
    function ($http, $rootScope) {
        var doRequest = function (path, query, data, method) {
            if (!path) return;

            var _method = method || 'GET'
                , _query = query || null
                , _data = data || null
            return $http({
                method: _method,
                url: path,
                query: _query,
                data: _data
            })
        }
        return {
            request: function(path, query, data, method){
                return doRequest.apply(this, arguments);
            },
            changeCampaignStatus: function(id, data, cb){
                this.request('admin/stats/' + id, null, data, 'PUT').success(cb)
            }

        }
    }
])

appServices.factory('Auth', ['$http', '$rootScope', '$cookieStore',  function($http, $rootScope, $cookieStore){
    return{
        checkRole: function(role){
            var  currentUser = $cookieStore.get('user')
            if (role == currentUser.role) return true
            return currentUser._id
        }
    }
}]);
