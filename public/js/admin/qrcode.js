//
////Wrap it within $(document).ready() to invoke the function after DOM loads.
//
//$(document).ready(function(){
//
//    $('#qrcodeholder').qrcode({
//        text    : "http://www.moreonfew.com/generate-qr-code-using-jquery",
//        render    : "canvas",  // 'canvas' or 'table'. Default value is 'canvas'
//        background : "#ffffff",
//        foreground : "#000000",
//        width : 100,
//        height: 100
//    });
//
//    });
//
