var _ = require('underscore')
    , env;

switch (process.env.NODE_ENV) {
    case 'production':
        env = 'production'
        break
    case 'qa':
        env = 'qa'
        break
    case 'mocha':
        env = 'mocha'
        break
    case 'staging':
        env = 'staging'
        break
    default:
        env = 'development'
        break
};


var config = {
    development: {
        facebook: {
            clientID: '1392608420964158'
            , clientSecret: '71f95fee1aad38a2b566071fb5a413c1'
            , callbackURL: "http://localhost:3000/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-9uod2c6sjrqttdg7i5o4sult8mi0e5pr.apps.googleusercontent.com'
            , clientSecret: 'HqfMWaCNKKdhDIYs0zag2o8-'
            , callbackURL: "http://localhost:3000/auth/google/callback"
        }
        , twitter: {
            consumerKey: 'DnmZHKmNNh8fJJFvb7W6oVbct'
            , consumerSecret: 'WjW7IQSW9ZIxH8YITJsNygkh1QjPDrxkhbXSQKaJplQhbOgeOA'
            , callbackURL: "http://localhost:3000/auth/twitter/callback"
        }
        , mongodb: 'localhost/venutopia'
        , url: "http://localhost:3000"
        , remote_url: "http://localhost:9000"
        , s3bucket: 'venutopia_dev'
    },
    staging: {
        facebook: {
            clientID: '269040456615071'
            , clientSecret: '5b717578b3251fd0018ce45b78757393'
            , callbackURL: "http://staging.venutopia.com/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-q1leffh1paroiv62i1b5igeooi0u2ovj.apps.googleusercontent.com'
            , clientSecret: '8YI83lJisi-AiLdtPLX_Wrxm'
            , callbackURL: "http://staging.venutopia.com/auth/google/callback"
        }
        , twitter: {
            consumerKey: '0UtVbCKW7CTW46wflV0fv0ojO'
            , consumerSecret: 'nfsjzxOGzkZtuilWs0lgaYVwbx3eVzPrz1lB51t8tQkZcWm80u'
            , callbackURL: "http://staging.venutopia.com/auth/twitter/callback"
        }
        , mongodb: 'heroku_app24706605:rh9tc6gomlps3vmgv9fqqam87i@ds033217.mongolab.com:33217/heroku_app24706605'
        , url: "http://staging.venutopia.com"
        , remote_url: "http://staging-mlb.venutopia.com"
        , s3bucket: 'venutopia_stg'
    },
    qa: {
        facebook: {
            clientID: '274049749447475'
            , clientSecret: '41ea834ded4fb5829281d56368ec3e7f'
            , callbackURL: "http://qa.venutopia.com/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-s06f4rujv8vva3e9esiondlhqarisveo.apps.googleusercontent.com'
            , clientSecret: 'h6oa-OL9cncrD5XXGNI6ubH0'
            , callbackURL: "http://qa.venutopia.com/auth/google/callback"
        }
        , twitter: {
            consumerKey: ''
            , consumerSecret: ''
            , callbackURL: "http://qa.venutopia.com/auth/twitter/callback"
        }
        , mongodb: 'heroku_app27321755:m43ji3fsno4lo9g2ri0700vpsb@ds037837.mongolab.com:37837/heroku_app27321755'
        , url: "http://qa.venutopia.com"
        , remote_url: "http://oakatpqa.bamnetworks.com"
        , s3bucket: 'venutopia_qa'
    },
    production: {
        facebook: {
            clientID: '269019393283844'
            , clientSecret: '199b3612696205bcba172cf281ce8e8f'
            , callbackURL: "http://prod.venutopia.com/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-j70jvpr4893n2o16ecibkp166hcrukq4.apps.googleusercontent.com'
            , clientSecret: 'q5dcFpGfuRCJqfvC7DOHJaBZ'
            , callbackURL: "http://prod.venutopia.com/auth/google/callback"
        }
        , twitter: {
            consumerKey: 'Ny6l1nLrQ7fl96nStEBoXrgMi'
            , consumerSecret: 'UGF3ZWj1JUTbMsr098e5wxnRYdcfEShTMJhGntcmAi3B1DT0tI'
            , callbackURL: "http://prod.venutopia.com/auth/twitter/callback"
        }
        , mongodb: 'heroku:XQsofdxMorMBgMa_L855ZbFw7faMkhPzbCFmYvzfUiscWvmBUQ3gIS6HJ-X0JvVp30XOHvd6UeKnNKBPKOnzQw@kahana.mongohq.com:10084/app27621205'
        , url: "http://prod.venutopia.com"
        , remote_url: "http://oakatp.bamnetworks.com"
        , s3bucket: 'venutopia_prod'
    },
    mocha: {
        facebook: {
            clientID: '269039706615146'
            , clientSecret: '804eafe2f065e19d44d1cabde62d9056'
            , callbackURL: "http://localhost:3000/auth/facebook/callback"
        }
        , google: {
            clientID: '1035862725548-9uod2c6sjrqttdg7i5o4sult8mi0e5pr.apps.googleusercontent.com'
            , clientSecret: 'HqfMWaCNKKdhDIYs0zag2o8-'
            , callbackURL: "http://localhost:3000/auth/google/callback"
        }
        , twitter: {
            consumerKey: 'DnmZHKmNNh8fJJFvb7W6oVbct'
            , consumerSecret: 'WjW7IQSW9ZIxH8YITJsNygkh1QjPDrxkhbXSQKaJplQhbOgeOA'
            , callbackURL: "http://localhost:3000/auth/twitter/callback"
        }
        , mongodb: 'localhost/venutopia-test'
        , url: "http://localhost:3000"
        , remote_url: "http://localhost:9000"
    }

}

module.exports = _.extend(config[env], {
    settings : {
        accessKeyId : 'AKIAIDODYK4XBOB4BVJQ'
        , secretAccessKey : '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'
        , bucket : config[env].s3bucket
        , amazon_path : 'https://s3-us-west-2.amazonaws.com/test-music-dunice/'
    }
})